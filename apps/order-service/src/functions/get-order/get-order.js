import { logger } from "@shop/logger";
import { AppError } from "@shop/app-error";

const logger = initlogger();

export const handler = async (event) => {
  const METHOD = "get-order.handler";

  if (!event) {
    throw new AppError("no event");
  }

  const orderDetails = {
    orderNo: 111,
    customerId: "A123",
    orderTotalAmount: 1.99,
    complete: false,
    total: 5,
  };

  try {
    logger.info(`${METHOD} - started`);
    return {
      statusCode: 200,
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(
        {
          message: orderDetails,
        },
        null,
        2
      ),
    };
  } catch (error) {
    logger.error(`${METHOD} - error: ${error}`);

    return {
      statusCode: 500,
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify("An error has occured", null, 2),
    };
  }
};
